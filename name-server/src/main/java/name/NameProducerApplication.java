package name;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.NameList;

/**
 * Created by cumacam on 15/12/16.
 */
@SpringBootApplication
@EnableEurekaClient
@RestController
@RequestMapping("/")
public class NameProducerApplication {

    private static final String[] nameList = new String[]{" Cuma", " Ahmet"};

    public static void main(String[] args) {
        SpringApplication.run(NameProducerApplication.class,args);
    }

    @GetMapping
    public String getName(){
        return nameList[(int) Math.round( Math.random() )];
    }
}
