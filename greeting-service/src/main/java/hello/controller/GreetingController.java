package hello.controller;

import hello.client.NameServerClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by cumacam on 15/12/16.
 */
@RestController
@RequestMapping
public class GreetingController {

    @Autowired
    private NameServerClient nameServerClient;

    @GetMapping("/hello")
    @PreAuthorize("#oauth2.hasScope('server')")
    public String sayHello(){
        return "Hello" + nameServerClient.getName();
    }

    @GetMapping("/demo")
    public String demo(){
        return "Demo";
    }
}
