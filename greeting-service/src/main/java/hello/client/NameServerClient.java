package hello.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by cumacam on 15/12/16.
 */
@FeignClient("name-server")
public interface NameServerClient {

    @RequestMapping(method = RequestMethod.GET, value = "/name")
    String getName();

}

